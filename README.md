php-mobile-detector
===================
### ***Detect the Mobile Phones Using the PHP*** ###


Usage
===================


#### Include File ####

```php
<?php include 'mobile.php'; ?> 
```



#### Detect Any Mobile Device ####


```php
<?php 
include 'mobile.php';
$mobile = new Mobile();

if ($mobile->isMobile) {
 header("Location: m.yoursite.com");
}

?>
```


#### Detect an Android Device ####

```php
<?php
include 'mobile.php';
$mobile = new Mobile();

if ($mobile->isAndroid) {
 header("Location: android.yoursite.com");
}

?>
```



#### Detect an iPhone Device ####

```php
<?php
include 'mobile.php';
$mobile = new Mobile();

if ($mobile->isIphone) {
 header("Location: iphone.yoursite.com");
}

?>
```




#### Other Detectable Devices ####

```php
Blackberry = isBlackberry;
Ipod = isIpod;
Ipad = isIpad;
Opera Mini = isOperaMini; 
Palm = isPalm;
Windows = isWindows; 
Other = isOther; 
```




#### Detect an Android Device and Any Other Device at once ####

```php
<?php
include 'mobile.php';
$mobile = new Mobile();

if ($mobile->isAndroid) {
 header("Location: android.yoursite.com");
} elseif ($mobile->isMobile) {
 header("Location: m.yoursite.com");
}

?>
```



Screenshoots
--

#### Not Detected ####
![Not Detected](http://i.imgur.com/KvsuXTH.png)

#### Detected an Android Device ####
![Android](http://i.imgur.com/ncT5Qng.png)

#### Detected an iPhone Device ####
![Iphone](http://i.imgur.com/AxH1gGE.png)

#### Detected an iPad Device ####
![Ipad](https://bitbucket.org/repo/7nb9a9/images/3740688536-m1.png)